package com.example.task54s40;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task54s40Application {

	public static void main(String[] args) {
		SpringApplication.run(Task54s40Application.class, args);
	}

}
